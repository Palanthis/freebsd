#!/bin/sh
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/palanthis
# License	:	BSD 2-Clause (Simplified) License
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Install xorg, sddm and Plasma
echo 'dbus_enable="YES"' >> /etc/rc.conf
echo 'hald_enable="YES"' >> /etc/rc.conf
echo 'sddm_enable="YES"' >> /etc/rc.conf
echo 'proc           /proc       procfs  rw  0   0' >> /etc/fstab
echo ". /usr/local/etc/xdg/xfce4/xinitrc" > ~/.xsession

pkg install -y xorg

pkg install -y x11/sddm

pkg install -y x11/xfce

pkg install -y sudo

pkg install -y nano

while true; do
    read -p "Do you wish to install this program?" yn
    case $yn in
        [Yy]* ) tar xzf x11.tar.gz -C /usr/share/ --overwrite; break;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
