#!/bin/sh
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/palanthis
# License	:	BSD 2-Clause (Simplified) License
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# VirtualBox Drivers
pkg install -y emulators/virtualbox-ose-additions
echo 'vboxguest_enable="YES"' >> /etc/rc.conf
echo 'vboxservice_enable="YES"' >> /etc/rc.conf
