#!/bin/sh
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/palanthis
# License	:	BSD 2-Clause (Simplified) License
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Create common folders
sudo pkg install -y xdg-user-dirs
xdg-user-dirs-update --force

# Install some apps
sudo pkg install -y geany
sudo pkg install -y conky
sudo pkg install -y plank
sudo pkg install -y vlc
sudo pkg install -y firefox
sudo pkg install -y qterminal
