#!/bin/sh
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/palanthis
# License	:	BSD 2-Clause (Simplified) License
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Install xorg, sddm and Plasma
echo 'dbus_enable="YES"' >> /etc/rc.conf
echo 'hald_enable="YES"' >> /etc/rc.conf
echo 'sddm_enable="YES"' >> /etc/rc.conf
echo 'proc           /proc       procfs  rw  0   0' >> /etc/fstab

pkg install -y xorg

pkg install -y x11/sddm

pkg install -y x11/kde5

pkg install -y sudo

pkg install -y nano
